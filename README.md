Django Page Slapper
===================

Django-Page-Slapper is a middleware that manages page creation using the Django Framework allowing blog like functionality.


Installation
------------

```pip install django-page-slapper```
